<?php

use Illuminate\Foundation\Application;
use Illuminate\Foundation\Configuration\Exceptions;
use Illuminate\Foundation\Configuration\Middleware;

return Application::configure(basePath: dirname(__DIR__))
    ->withRouting(
        web: __DIR__.'/../routes/web.php',
        commands: __DIR__.'/../routes/console.php',
        health: '/up',
        then:function () {
            \Illuminate\Support\Facades\Route::middleware("web")
                ->group(function () {
                    require base_path('app/Features/Categories/Http/Routes.php');
                    require base_path('app/Features/Products/Http/Routes.php');
                    require base_path('app/Features/Orders/Http/Routes.php');
                });
        },

    )
    ->withMiddleware(function (Middleware $middleware) {
        //
    })
    ->withExceptions(function (Exceptions $exceptions) {
        //
    })->create();
