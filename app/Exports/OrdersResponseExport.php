<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class OrdersResponseExport implements FromCollection, WithHeadings, ShouldAutoSize
{

    protected array $errorBag;

    public function __construct(array $errorBag)
    {
        $this->errorBag = $errorBag;
    }

    public function collection()
    {
        return new Collection($this->errorBag);
    }

    public function headings(): array
    {
        return [
            'Customer Name',
            'Customer Email',
            'Order Date',
            'Product id',
            'Quantity',
            'Status',
            'Errors',
        ];
    }
}
