<?php

namespace App\Exports;

use App\Features\Orders\Domain\Models\Order;
use App\Features\Orders\Domain\Models\OrderItem;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class OrderItemSheetExport implements FromCollection, WithHeadings, ShouldAutoSize, WithTitle
{
    protected Collection $ids;
    public function __construct(Collection $ids)
    {
        $this->ids = $ids;
    }

    /**
     * @return Collection
     */
    public function collection(): Collection
    {
        return OrderItem::findMany($this->ids);
    }

    public function headings(): array
    {
        return [
            "Id",
            "Order Id",
            "Product Id",
            "Quantity",
            "Price Per Unit",
            "Created At",
            "Updated At"
        ];
    }

    public function title(): string
    {
        return "Order Items";
    }
}
