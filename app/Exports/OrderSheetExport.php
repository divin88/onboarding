<?php

namespace App\Exports;

use App\Features\Orders\Domain\Models\Order;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class OrderSheetExport implements FromCollection, WithHeadings, ShouldAutoSize, WithTitle
{
    protected Collection $ids;
    public function __construct(Collection $ids)
    {
        $this->ids = $ids;
    }

    /**
     * @return Collection
     */
    public function collection(): Collection
    {
         return Order::findMany($this->ids);
    }

    public function headings(): array
    {
        return [
            "Id",
            "Customer Name",
            "Customer Email",
            "Order Date",
            "Net Total",
            "Order Status",
            "Created At",
            "Updated At"
        ];
    }

    public function title(): string
    {
        return "Orders";
    }
}
