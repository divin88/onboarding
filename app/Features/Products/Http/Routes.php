<?php
\Illuminate\Support\Facades\Route::resource(
    "/products", \App\Features\Products\Http\Controllers\Admin\V1\Controllers\ProductsController::class
)->except('show');

\Illuminate\Support\Facades\Route::get(
    '/products/export',
    [\App\Features\Products\Http\Controllers\Admin\V1\Controllers\ProductsController::class, 'export']
)->name("products.export");

\Illuminate\Support\Facades\Route::post(
    '/products/import',
    [\App\Features\Products\Http\Controllers\Admin\V1\Controllers\ProductsController::class, 'import']
)->name("products.import");

\Illuminate\Support\Facades\Route::get(
    '/products/sample-export',
    [\App\Features\Products\Http\Controllers\Admin\V1\Controllers\ProductsController::class, 'downloadSample']
)->name("products.downloadSample");

//\Illuminate\Support\Facades\Route::get(
//    'products/error-logs',
//    [\App\Features\Products\Http\Controllers\Admin\V1\Controllers\ProductsController::class, 'mail']
//)->name('products.mail');
