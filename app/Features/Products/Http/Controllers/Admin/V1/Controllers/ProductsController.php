<?php

namespace App\Features\Products\Http\Controllers\Admin\V1\Controllers;

use App\DataTables\ProductsDataTable;
use App\Features\Categories\Domain\Models\Category;
use App\Features\Products\Domain\Exports\CategoryProductSampleExport;
use App\Features\Products\Domain\Exports\ProductsExport;
use App\Features\Products\Domain\Models\Product;
use App\Features\Products\Http\Controllers\Admin\V1\Actions\ProductsAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class ProductsController extends Controller
{

    public function __construct(private ProductsAction $productsAction) {}
    public function index()
    {
        return view("products.index");
    }

    public function create()
    {
        $categories = Category::select("name", "id")
            ->where("is_active", "=", true)
            ->get();
        return view("products.create", compact(["categories"]));
    }

    public function store(CreateProductRequest $request)
    {
        $result = $request["outer_group"][0]["inner_group"];
//        dd($result);
        try {
//            dd($category_id);
            $count = count($result);
//            dd($count, $result);
            for ($i=0; $i < $count; $i++) {
                $productData["category_id"] = $result[$i]["category_id"];
                $productData["name"] = $result[$i]["name"];
                $productData["description"] = $result[$i]["description"];
                $productData["price"] = $result[$i]["price"];
                $productData["stock"] = $result[$i]["stock"];
                $productData["is_active"] = $result[$i]["is_active"];
                $this->productsAction->persistProduct($productData);
            }
            session()->flash("success", "Product created successfully!");
        } catch (\Exception $exception) {
            Log::info($exception);
            session()->flash("error", "Error while creating!");
        }
        return redirect()->route("products.index");
    }

    public function edit(Product $product)
    {
        $categories = Category::select("name", "id")
            ->where("is_active", "=", true)
            ->get();
        return view("products.edit", compact(["product", "categories"]));
    }

    public function update(Product $product, UpdateProductRequest $request)
    {
        try {
            $data["category_id"] = $request->category_name;
            $data["name"] = $request->name;
            $data["description"] = $request->description;
            $data["price"] = $request->price;
            $data["stock"] = $request->stock;
            $data["is_active"] = $request->is_active === "1";
            $this->productsAction->updateProduct($product, $data);
            session()->flash("success", "Product updated successfully!");

        } catch (\Exception $exception) {
            Log::info($exception);
            session()->flash("error", "Error while updating!");
        }
        return redirect()->back();
    }

//    public function destroy(Product $product)
//    {
//        $product->delete();
//
//        session()->flash('success', 'Product deleted successfully...');
//        return redirect()->route('products.index');
//    }


    public function import(Request $request)
    {
        $file = $request->file('file');
        $storedFile = $file->storeAs(
            "temp/import-files",
            "temp".uniqid().".".$file->getClientOriginalExtension()
        );

        if(!$file) {
            Log::error("File not found");
            return redirect(route("products.index"));
        }
        $this->productsAction->importProducts($storedFile);
        return redirect(route("products.index"))
            ->with("success", "Import has begun successfully, will be completed in some time!");
    }

    public function export(Request $request)
    {
        $data = $request->input();
//        dd($data);
        $exportData["name"] = $data["hidden_name_input"];
        $exportData["description"] = $data["hidden_description_input"];
        $exportData["price"] = $data["hidden_price_input"];
        $exportData["stock"] = $data["hidden_stock_input"];
        $exportData["is_active"] = $data["hidden_is_active_input"];
        $exportData["created_at"] = $data["hidden_created_at_input"];
        $exportData["updated_at"] = $data["hidden_updated_at_input"];
        return Excel::download(new ProductsExport($exportData), 'products.xlsx');
    }

    public function downloadSample()
    {
        return (new CategoryProductSampleExport())
            ->download("sample.xlsx");
    }

    public function getProducts(Request $request)
    {
//        dd($request);
        $searchValue = $request->search["value"];
        $orderBy = $request->order;
        $start = $request->start;
        $length = $request->length;
        $draw = $request->draw;

        $searchData["name_input"] = $request->search["name_input"];
        $searchData["description_input"] = $request->search["description_input"];
        $searchData["price_input"] = $request->search["price_input"];
        $searchData["stock_input"] = $request->search["stock_input"];
        $searchData["is_active_input"] = $request->search["is_active_input"];
        $searchData["created_at_input"] = $request->search["created_at_input"];
        $searchData["updated_at_input"] = $request->search["updated_at_input"];


        $filteredData = Product::name($searchData["name_input"])->description($searchData["description_input"])->price($searchData["price_input"])->stock($searchData["stock_input"])->isActive($searchData["is_active_input"])->createdAt($searchData["created_at_input"])->updatedAt($searchData["updated_at_input"])->order($orderBy)->limitBy($start, $length)->get();
        $numberOfTotalRecords = Product::count();
        $numberOfFilteredRecords = Product::search($searchValue)->count();

        $data = [];
        for($i = 0; $i < sizeof($filteredData); $i++) {
            $subarray = [];
            $subarray[] = $filteredData[$i]->id;
            $subarray[] = $filteredData[$i]->category_id;
            $subarray[] = $filteredData[$i]->name;
            $subarray[] = $filteredData[$i]->description;
            $subarray[] = $filteredData[$i]->price;
            $subarray[] = $filteredData[$i]->stock;
            $subarray[] = $filteredData[$i]->is_active;
            $subarray[] = $filteredData[$i]->created_at->format('Y-m-d');
            $subarray[] = $filteredData[$i]->updated_at->format('Y-m-d');
            $data[] = $subarray;
        }

        $result = array(
            "draw" => $draw,
            "recordsTotal" => $numberOfTotalRecords,
            "recordsFiltered" => $numberOfFilteredRecords,
            "data" => $data
        );

        return json_encode($result);
    }

}
