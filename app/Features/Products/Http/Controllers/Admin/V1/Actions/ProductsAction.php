<?php

namespace App\Features\Products\Http\Controllers\Admin\V1\Actions;

use App\Features\Products\Domain\Models\Product;
use App\Jobs\HandleErrorRecordsJob;
use Illuminate\Support\Facades\Bus;

class ProductsAction
{
     public function persistProduct(array $data)
    {
//        dd($data);
        Product::persistProduct($data);
    }

    public function updateProduct(Product $product, array $data): Product
    {
        return $product->updateProduct($data);
    }

    public function importProducts(string $storedFile): void
    {
        Bus::chain([
           new HandleErrorRecordsJob($storedFile),
        ])->dispatch();
//        Excel::download(new ErrorRecordsExport($import->errorBag), "error_records.xlsx");
//            dd($import->failures);
//            if($import->failures->isNotEmpty()) {
//                return back()->withFailures();
//            }

//            foreach ($import->failures() as $failure) {
//                $failure->row();
//                $failure->attribute();
//                ($failure->errors());
//                $failure->values();
//            }
//        $errors = $import->getErrorBag();
//        dd($errors);
//        if (!empty($errors)) {
//            $errorFilePath = 'error_records_' . time() . '.xlsx';
//            $errorExport = new ErrorRecordsExport($errors);
//            return Excel::store($errorExport, $errorFilePath, "public");
//        }
//        return redirect()->back()->with('success', 'Imported successfully with no errors.');
    }
}
