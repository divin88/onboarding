<?php

namespace App\Features\Products\Domain\Models\Constants;

interface ProductConstants
{
    public const PERSIST_VALIDATION_RULES = [
        "name" => "required|max:255|string",
        "category_id" => "required|exists:categories,id",
        "description" => "nullable|string",
        "price" => "required|numeric",
        "stock" => "required|integer",
        "is_active" => "required|boolean",
    ];

    public const UPDATE_VALIDATION_RULES = [
        "name" => "required|max:255|string",
        "category_id" => "required|exists:categories,id",
        "description" => "nullable|string",
        "price" => "required|numeric",
        "stock" => "required|integer",
        "is_active" => "required|boolean",
    ];
}
