<?php

namespace App\Features\Products\Domain\Models;

use App\Features\Categories\Domain\Models\Category;
use App\Features\Products\Domain\Models\Constants\ProductConstants;
use App\Helpers\Models\BaseModel;
use App\Helpers\Services\Utils;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\DB;

class Product extends BaseModel implements ProductConstants
{
    protected $table = "products";

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }
    public static function persistProduct(array $data): self
    {
//        dd($data);
        Utils::validateOrThrow($data, self::persistRules());
        return DB::transaction(function () use ($data) {
            return Product::create($data);
        });
    }

    public function updateProduct(array $data): self
    {
        Utils::validateOrThrow($data, self::updateRules());
        DB::transaction(function () use ($data) {
            $this->update($data);
        });
        return $this;
    }

    public static function persistRules(): array
    {
        return self::PERSIST_VALIDATION_RULES;
    }

    public static function updateRules(): array
    {
        return self::UPDATE_VALIDATION_RULES;
    }

    public function scopeSearch($query, $searchValue)
    {
        if($searchValue){
            return $query->where('name', 'LIKE', "%{$searchValue}%")
//                ->orWhere('category_id', 'LIKE', "%{$searchValue}%")
                ->orWhere('description', 'LIKE', "%{$searchValue}%")
                ->orWhere('price', 'LIKE', "%{$searchValue}%")
                ->orWhere('stock', 'LIKE', "%{$searchValue}%")
                ->orWhere('is_active', 'LIKE', "%{$searchValue}%")
                ->orWhere('created_at', 'LIKE', "%{$searchValue}%")
                ->orWhere('updated_at', 'LIKE', "%{$searchValue}%");
        }
        return $query;
    }
    public function scopeName($query, $searchValue)
    {
        if($searchValue) {
            return $query->where('name', 'LIKE', "%{$searchValue}%");
        }
        return $query;
    }
    public function scopeDescription($query, $searchValue)
    {
        if($searchValue) {
            return $query->where('description', 'LIKE', "%{$searchValue}%");
        }
        return $query;
    }
    public function scopePrice($query, $searchValue)
    {
        if($searchValue) {
            return $query->where('price', 'LIKE', "%{$searchValue}%");
        }
        return $query;
    }
    public function scopeStock($query, $searchValue)
    {
        if($searchValue) {
            return $query->where('stock', 'LIKE', "%{$searchValue}%");
        }
        return $query;
    }
    public function scopeIsActive($query, $searchValue)
    {
        if(isset($searchValue)) {
            return $query->where('is_active', '=', "$searchValue");
        }
        return $query;
    }
    public function scopeCreatedAt($query, $searchValue)
    {
        if($searchValue) {
            return $query->where('created_at', 'LIKE', "%{$searchValue}%");
        }
        return $query;
    }
    public function scopeUpdatedAt($query, $searchValue)
    {
        if($searchValue) {
            return $query->where('updated_at', 'LIKE', "%{$searchValue}%");
        }
        return $query;
    }

    public function scopeOrder($query, $orderBy)
    {
        if($orderBy) {
            $columns = ["id", "category_id", "name", "description", "price", "stock", "is_active", "created_at", "updated_at"];
            $query->orderBy($columns[$orderBy[0]['column']], $orderBy[0]['dir']);
        }
        return $query;
    }

    public function scopeLimitBy($query, $start = 0, $length = 10)
    {
        if($length == -1)
            return $query;

        return $query->offset($start)->limit($length);
    }

}
