<?php

namespace App\Features\Products\Domain\Imports;

use App\Features\Products\Domain\Models\Product;
use App\Features\Products\Http\Controllers\Admin\V1\Actions\ProductsAction;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProductsImport implements ToCollection, WithHeadingRow
{
    use Importable;
    protected array $errorBag = [];

    private ProductsAction $productsAction;
    public function __construct()
    {
        $this->productsAction = new ProductsAction();
    }

    protected array $rules = [
        "id" => "integer",
        "category_id" => "required|exists:categories,id",
        "name" => "required|max:255|string",
        "description" => "nullable|string",
        "price" => "required|numeric",
        "stock" => "required|integer",
        "is_active" => "boolean",
    ];
    public function makeData(Collection $row): array
    {
        return [
            "id" => trim($row["id"]),
            "category_id" => trim($row["category_id"]),
            "name" => trim($row["name"]),
            "description" => trim($row["description"]),
            "price" => trim($row["price"]),
            "stock" => trim($row["stock"]),
            "is_active" => trim($row["is_active"]) === "1" ? "1" : "0",
        ];
    }
    public function collection(Collection $collection): void
    {
        foreach ($collection as $row){
            $rowData = $this->makeData($row);
            $validator = Validator::make($rowData, $this->rules);
//            dd($row);
            if ($validator->fails()) {
                Log::error("Failed to import product with name: {$rowData['name']} and category id: {$rowData['category_id']}");
                $errors = implode(', ', $validator->errors()->all());
                $this->errorBag[] = [
                    $rowData['id'],
                    $rowData['category_id'],
                    $rowData['name'],
                    $rowData['description'],
                    $rowData['price'],
                    $rowData['stock'],
                    $rowData['is_active'],
                    "FAIL",
                    $errors
                ];
            } else {
                Log::info("Imported product with id: {$rowData['id']}");
                $this->errorBag[] = [
                    $rowData['id'],
                    $rowData['category_id'],
                    $rowData['name'],
                    $rowData['description'],
                    $rowData['price'],
                    $rowData['stock'],
                    $rowData['is_active'],
                    "PASS",
                    "-"
                ];
                $this->productsAction->persistProduct($rowData);
            }
        }
    }

    public function getErrorBag(): array
    {
        return $this->errorBag;
    }
}
