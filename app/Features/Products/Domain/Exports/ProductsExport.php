<?php

namespace App\Features\Products\Domain\Exports;

use App\Features\Products\Domain\Models\Product;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProductsExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    protected array $exportData;
    public function __construct(array $exportData)
    {
        $this->exportData = $exportData;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Product::name($this->exportData["name"])->description($this->exportData["description"])->price($this->exportData["price"])->stock($this->exportData["stock"])->isActive($this->exportData["is_active"])->createdAt($this->exportData["created_at"])->updatedAt($this->exportData["updated_at"])->get();
    }

    public function headings(): array
    {
        return [
            "ID",
            "Category ID",
            "Name",
            "Description",
            "Price",
            "Stock",
            "Is Active",
            "Created At",
            "Updated At"
        ];
    }
}
