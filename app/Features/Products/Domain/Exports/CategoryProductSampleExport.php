<?php

namespace App\Features\Products\Domain\Exports;

use App\Features\Categories\Domain\Exports\CategoriesExport;
use App\Features\Categories\Domain\Exports\CategorySampleExport;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class CategoryProductSampleExport implements WithMultipleSheets
{

    use Exportable;
    public function sheets(): array
    {
        $sheets = [];

        $sheets[0] = new CategorySampleExport();
        $sheets[1] = new ProductSampleExport();
        return $sheets;
    }
}
