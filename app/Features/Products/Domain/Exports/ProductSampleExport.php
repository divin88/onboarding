<?php

namespace App\Features\Products\Domain\Exports;

use App\Features\Products\Domain\Models\Product;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class ProductSampleExport implements WithHeadings, ShouldAutoSize, FromQuery, WithTitle
{
    public function query()
    {
        return Product::query()->get();
    }

    public function headings(): array
    {
        return [
            "ID",
            "Category ID",
            "Name",
            "Description",
            "Price",
            "Stock",
            "Is Active",
            "Created At",
            "Updated At"
        ];
    }

    public function title(): string
    {
        return "Products";
    }
}
