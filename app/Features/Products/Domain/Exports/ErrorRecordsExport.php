<?php

namespace App\Features\Products\Domain\Exports;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ErrorRecordsExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    protected array $errorBag;

    public function __construct(array $errorBag)
    {
        $this->errorBag = $errorBag;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return new Collection($this->errorBag);
    }

    public function headings(): array
    {
        return [
            'Id',
            'Category id',
            'Name',
            'Description',
            'Price',
            'Stock',
            'Is Active',
            'Status',
            'Errors',
        ];
    }
}
