<?php

namespace App\Features\Orders\Http\Controllers\Admin\V1\Actions;

use App\Features\Orders\Domain\Models\OrderItem;
use App\Features\Products\Domain\Models\Product;

class OrderItemsAction
{
    public function persistOrderItems($order_id, array $orders)
    {
        for ($i=0; $i<count($orders); $i++) {
            $stock = Product::query("stock")->where("id", $orders[$i]["product_id"])->pluck("stock");
//            dd($stock[0]);
            $orderItems["order_id"] = $order_id;
            $orderItems["product_id"] = $orders[$i]["product_id"];
            $orderItems["quantity"] = $orders[$i]["quantity"];
            $orderItems["unit_price"] = $orders[$i]["unit_price"];
//            dd($orderItems["quantity"], $stock[$i]);
//            dd($orderItems["quantity"] <= $stock[$i]);
            if((int)$orderItems["quantity"] <= $stock[0])
                OrderItem::persistOrderItem($orderItems);
            else
                session()->flash("error", "Error while placing your order!");
        }
        return redirect()->route("orders.create");
    }
}
