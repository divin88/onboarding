<?php

namespace App\Features\Orders\Http\Controllers\Admin\V1\Actions;

use App\Exports\OrdersResponseExport;
use App\Features\Orders\Domain\Models\Order;
use App\Features\Products\Domain\Models\Product;
use App\Imports\OrdersImport;
use App\Mail\OrdersResponseMail;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class OrdersAction
{
    public function persistOrder(array $data): Order
    {
        return Order::persistOrder($data);
    }

    public function store(array $data)
    {
        try {
            DB::beginTransaction();

            $order = Order::create([
                'customer_name' => $data['customer_name'],
                'customer_email' => $data['customer_email'],
                'order_date' => $data['order_date'],
                'order_status' => 'pending',
                'net_total' => 0,
            ]);

            $netTotal = 0;

            foreach ($data['orders'] as $item) {
                $product = Product::findOrFail($item['product_id']);

                $totalPrice = ($item['quantity'] * $product->price);

                $orderItem = $order->orderItems()->create([
                    'product_id' => $item['product_id'],
                    'quantity' => $item['quantity'],
                    'unit_price' => $product->price,
                ]);
                $netTotal += $totalPrice;
                $product->decrement('stock', $item['quantity']);
            }
            $order->update(['net_total' => $netTotal]);
            DB::commit();

            return $order;
        } catch (\Exception $exception) {
            DB::rollback();
            throw $exception;
        }
    }


    public function export(array $data): Collection
    {
        $ids = Order::findMany(explode(',', $data["form_order_id"]))->pluck('id');
        return $ids;
    }

    public function updateOrder(array $data, $id)
    {
//        dd($data);
        $orderData = [
            'customer_name' => $data['customer_name'],
            'customer_email' => $data['customer_email'],
            'order_date' => $data['order_date'],
            'order_status' => $data['order_status'],
            'net_total' => $data['net_total'],
        ];

        $orderItems = $data['orders'];

        $order = Order::findOrFail($id);
        $order->updateProduct($orderData);

        // purane items del
        $order->orderItems()->delete();
        // create new
        foreach ($orderItems as $item) {
            $product = Product::find($item['product_id']);
            $order->orderItems()->create([
                'product_id' => $item['product_id'],
                'quantity' => $item['quantity'],
                'unit_price' => $product->price
            ]);
        }
    }

    public function import(array $data, string $storedFile)
    {
        $orderData = [
            'customer_name' => $data['customer_name'],
            'customer_email' => $data['customer_email'],
            'order_date' => $data['order_date'],
        ];
        $order = Order::create($orderData);

        $import = new OrdersImport($orderData, $order->id);
        Excel::import($import, $storedFile, "public");

        $errors = $import->getErrorBag();
        $responseFilePath = 'orders_response_' . now()->format('Y-m-d_H-i-s') . '.xlsx';
        Excel::store((new OrdersResponseExport($errors)), $responseFilePath, "public");

        Mail::to("divij.ningu2026@onestopengineering.in")->send(new OrdersResponseMail($responseFilePath));

    }
}
