<?php

namespace App\Features\Orders\Http\Controllers\Admin\V1\Controllers;

use App\Features\Categories\Domain\Models\Category;
use App\Features\Orders\Domain\Exports\MultipleSheetSampleForOrdersExport;
use App\Features\Orders\Domain\Exports\OrdersExport;
use App\Features\Orders\Domain\Models\Order;
use App\Features\Orders\Http\Controllers\Admin\V1\Actions\OrderItemsAction;
use App\Features\Orders\Http\Controllers\Admin\V1\Actions\OrdersAction;
use App\Features\Products\Domain\Models\Product;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class OrdersController extends Controller
{
    public function __construct(private OrdersAction $ordersAction, private OrderItemsAction $orderItemsAction){}

    public function show($id)
    {
        $order = Order::with('orderItems')->findOrFail($id);
        return view("orders.show", compact('order'));
    }
    public function index()
    {
        return view("orders.index");
    }
    public function create()
    {
        $categories = Category::query()->select(['id', 'name'])->where("is_active", "=", true)->get();
        return view('orders.create', compact(['categories']));
    }

    public function getProducts(Request $request)
    {
        $category_id = $request->category_id;
        $products = Product::query()->select(['id', 'name'])->where("category_id", "=", $category_id)->where("is_active", "=", true)->get();
        return json_encode($products);
    }

    public function getProductQuantityAndPrice(Request $request)
    {
        $product_id = $request->product_id;
        $response = Product::query()->select(['stock', 'price'])->where("id", "=", $product_id)->get();
        return json_encode($response);
    }

    public function store(CreateOrderRequest $request)
    {
        try{
            $order = $this->ordersAction->store($request->validated());
            session()->flash("success", "Order placed successfully!");
        } catch (\Exception $exception) {
            Log::info($exception);
            session()->flash("error", "Error while placing your order!");
        }
        return redirect()->route("orders.index");
    }

    public function edit($id)
    {
        $order = Order::with('orderItems')->findOrFail($id);
        $categories = Category::query()->select(['id', 'name'])->where("is_active", "=", true)->get();
        $products = Product::query()->select(['id', 'name'])->where("is_active", "=", true)->get();
        return view("orders.edit", compact(['order', 'categories', 'products']));
    }

    public function update(UpdateOrderRequest $request, $id)
    {
        $this->ordersAction->updateOrder($request->validated(), $id);
        return redirect()->route('orders.index')->with('success', 'Order updated successfully.');
    }

    public function getOrders(Request $request)

    {
//        dd($request->search);
        $searchValue = $request->search["value"];
        $orderBy = $request->order;
        $start = $request->start;
        $length = $request->length;
        $draw = $request->draw;
//        dd($searchValue. $orderBy);

        $searchData["id"] = $request->search["id"];
        $searchData["customer_name"] = $request->search["customer_name"];
        $searchData["customer_email"] = $request->search["customer_email"];
        $searchData["order_date"] = $request->search["order_date"];
        $searchData["net_total"] = $request->search["net_total"];
        $searchData["order_status"] = $request->search["order_status"];
        $searchData["created_at"] = $request->search["created_at"];
        $searchData["updated_at"] = $request->search["updated_at"];
//        dd($searchData);

        $filteredData = Order::id($searchData["id"])->customerName($searchData["customer_name"])->customerEmail($searchData["customer_email"])->orderDate($searchData["order_date"])->netTotal($searchData["net_total"])->orderStatus($searchData["order_status"])->createdAt($searchData["created_at"])->updatedAt($searchData["updated_at"])->order($orderBy)->limitBy($start, $length)->get();
        $numberOfTotalRecords = Order::count();
        $numberOfFilteredRecords = Order::search($searchValue)->count();

        $data = [];
        for($i = 0; $i < sizeof($filteredData); $i++) {
            $subarray = [];
            $subarray[] = $filteredData[$i]->id;
            $subarray[] = $filteredData[$i]->customer_name;
            $subarray[] = $filteredData[$i]->customer_email;
            $subarray[] = $filteredData[$i]->order_date;
            $subarray[] = $filteredData[$i]->net_total;
            $subarray[] = $filteredData[$i]->order_status;
            $subarray[] = $filteredData[$i]->created_at->format('Y-m-d');
            $subarray[] = $filteredData[$i]->updated_at->format('Y-m-d');
            if ($filteredData[$i]->order_status === "pending") {
                $subarray[] =
                    '<div><a href="' . route('orders.show', ['order' => $filteredData[$i]->id]) . '" class="btn btn-outline-primary mb-1">Show Details</a></div>' .
                    '<div><a href="' . route('orders.edit', ['order' => $filteredData[$i]->id]) . '" class="btn btn-outline-success mb-1">Edit order</a></div>' .
                    '<div><a href="' . route('orders.create') . '" class="btn btn-outline-danger">Cancel order</a></div>';
            } else {
                $subarray[] =
                    '<div><a href="' . route('orders.show', ['order' => $filteredData[$i]->id]) . '" class="btn btn-outline-primary mb-1">Show Details</a></div>';
            }

            $data[] = $subarray;
        }

        $result = array(
            "draw" => $draw,
            "recordsTotal" => $numberOfTotalRecords,
            "recordsFiltered" => $numberOfFilteredRecords,
            "data" => $data
        );

        return json_encode($result);
    }

    public function sample(Request $request)
    {
        return (new MultipleSheetSampleForOrdersExport())->download("Orders_Skeleton.xlsx");
    }

    public function export(Request $request)
    {
        $data = $request->input();
        $ids = $this->ordersAction->export($data);
        $orderItemIds = Order::with("orderItems")->findMany($ids)->pluck("orderItems")->flatten()->pluck('id');
//        dd($orderItemIds);
        return Excel::download(new OrdersExport($ids, $orderItemIds), 'orders.xlsx');
    }

    public function import(Request $request)
    {
        $file = $request->file('file');
        $storedFile = $file->storeAs(
            "temp/import-files",
            "temp".uniqid().".".$file->getClientOriginalExtension()
        );

        if(!$file) {
            Log::error("File not found");
            return redirect(route("orders.index"));
        }
        $this->ordersAction->import($request->input(), $storedFile);
        return redirect(route("orders.index"))
            ->with("success", "Import has begun successfully, will be completed in some time!");
    }
}
