<?php

\Illuminate\Support\Facades\Route::resource(
    "/orders", \App\Features\Orders\Http\Controllers\Admin\V1\Controllers\OrdersController::class
);

\Illuminate\Support\Facades\Route::post(
    "/orders/get-products",
    [\App\Features\Orders\Http\Controllers\Admin\V1\Controllers\OrdersController::class, 'getProducts']
)->name("orders.getProducts");

\Illuminate\Support\Facades\Route::post(
    "/orders/get-product-quantity-and-price",
    [\App\Features\Orders\Http\Controllers\Admin\V1\Controllers\OrdersController::class, 'getProductQuantityAndPrice']
)->name("orders.getProductQuantityAndPrice");

\Illuminate\Support\Facades\Route::post(
    "/orders/sample",
    [\App\Features\Orders\Http\Controllers\Admin\V1\Controllers\OrdersController::class, 'sample']
)->name("orders.sample");

\Illuminate\Support\Facades\Route::post(
    "/orders/get-orders",
    [\App\Features\Orders\Http\Controllers\Admin\V1\Controllers\OrdersController::class, 'getOrders']
)->name("orders.get-orders");

\Illuminate\Support\Facades\Route::post(
    '/orders/export',
    [\App\Features\Orders\Http\Controllers\Admin\V1\Controllers\OrdersController::class, 'export']
)->name("orders.export");

\Illuminate\Support\Facades\Route::post(
    '/orders/import',
    [\App\Features\Orders\Http\Controllers\Admin\V1\Controllers\OrdersController::class, 'import']
)->name("orders.import");
