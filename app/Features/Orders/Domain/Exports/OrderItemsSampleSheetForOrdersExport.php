<?php

namespace App\Features\Orders\Domain\Exports;

use App\Features\Categories\Domain\Models\Category;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class OrderItemsSampleSheetForOrdersExport implements WithTitle, WithHeadings
{

    public function headings(): array
    {
        return [
            "product_id",
            "quantity",
        ];
    }

    public function title(): string
    {
        return "Order Items";
    }
}
