<?php

namespace App\Features\Orders\Domain\Exports;

use App\Exports\OrderItemSheetExport;
use App\Exports\OrderSheetExport;
use App\Features\Orders\Domain\Models\Order;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithTitle;

class OrdersExport implements WithMultipleSheets
{
    protected $orderId;
    protected $itemsId;
    public function __construct(Collection $orderId, Collection $itemsId)
    {
        $this->orderId = $orderId;
        $this->itemsId = $itemsId;
    }

    public function sheets(): array
    {
        $sheets = [];

        $sheets[0] = new OrderSheetExport($this->orderId);
        $sheets[1] = new OrderItemSheetExport($this->itemsId);
        return $sheets;
    }
}
