<?php

namespace App\Features\Orders\Domain\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class MultipleSheetSampleForOrdersExport implements WithMultipleSheets
{

    use Exportable;
    public function sheets(): array
    {
        $sheets = [];

        $sheets[0] = new OrderItemsSampleSheetForOrdersExport();
        $sheets[1] = new ProductSampleSheetForOrdersExport();
        return $sheets;
    }
}
