<?php

namespace App\Features\Orders\Domain\Exports;

use App\Features\Products\Domain\Models\Product;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class ProductSampleSheetForOrdersExport implements WithHeadings, ShouldAutoSize, FromQuery, WithTitle
{
    public function query()
    {
        return Product::query()->join("categories", "products.category_id", "=", "categories.id")
            ->where("products.is_active", true)
            ->select("products.id", "categories.name as category_name", "products.name");
    }

    public function headings(): array
    {
        return [
            "Product ID",
            "Category Name",
            "Product Name"
        ];
    }

    public function title(): string
    {
        return "Products";
    }
}
