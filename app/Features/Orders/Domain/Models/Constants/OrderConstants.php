<?php

namespace App\Features\Orders\Domain\Models\Constants;

interface OrderConstants
{
    public const PERSIST_VALIDATION_RULES = [
        "customer_name" => "required:true|min:2|max:255|string",
        "customer_email" => "required:true|email",
        "order_date" => "required:true|date",
        "net_total" => "required:true|nullable|numeric",
    ];

    public const UPDATE_VALIDATION_RULES = [
        "customer_name" => "required:true|min:2|max:255|string",
        "customer_email" => "required:true|email",
        "order_date" => "required:true|date",
        "net_total" => "required:true|nullable|numeric",
    ];
}
