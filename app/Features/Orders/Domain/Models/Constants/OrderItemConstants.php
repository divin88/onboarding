<?php

namespace App\Features\Orders\Domain\Models\Constants;

interface OrderItemConstants
{
    public const PERSIST_VALIDATION_RULES = [
        "order_id" => "required|exists:orders,id",
        "product_id" => "required|exists:products,id",
        "quantity" => "required|integer",
        "unit_price" => "required",
    ];

    public const UPDATED_VALIDATION_RULES = [
        "order_id" => "required|exists:orders,id",
        "product_id" => "required|exists:products,id",
        "quantity" => "required|integer",
        "unit_price" => "required",
    ];

}
