<?php

namespace App\Features\Orders\Domain\Models;

use App\Features\Orders\Domain\Models\Constants\OrderConstants;
use App\Helpers\Models\BaseModel;
use App\Helpers\Services\Utils;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;

class Order extends BaseModel implements OrderConstants
{
    protected $table = "orders";

    public function orderItems(): HasMany
    {
        return $this->hasMany(OrderItem::class);
    }

    public static function persistOrder(array $data): self
    {
//        dd($data);
        Utils::validateOrThrow($data, self::persistRules());
        return DB::transaction(function () use ($data) {
            return Order::create($data);
        });
    }

    public function updateProduct(array $data): self
    {
        Utils::validateOrThrow($data, self::updateRules());
        DB::transaction(function () use ($data) {
            $this->update($data);
        });
        return $this;
    }

    public static function persistRules(): array
    {
        return self::PERSIST_VALIDATION_RULES;
    }

    public static function updateRules(): array
    {
        return self::UPDATE_VALIDATION_RULES;
    }

    public function scopeSearch($query, $searchValue)
    {
        if($searchValue){
            return $query->where('id', 'LIKE', "%{$searchValue}%")
                ->orWhere('customer_name', 'LIKE', "%{$searchValue}%")
                ->orWhere('customer_email', 'LIKE', "%{$searchValue}%")
                ->orWhere('order_date', 'LIKE', "%{$searchValue}%")
                ->orWhere('net_total', 'LIKE', "%{$searchValue}%")
                ->orWhere('order_status', 'LIKE', "%{$searchValue}%")
                ->orWhere('created_at', 'LIKE', "%{$searchValue}%")
                ->orWhere('updated_at', 'LIKE', "%{$searchValue}%");
        }
        return $query;
    }

    public function scopeId($query, $id) {
        if($id)
            return $query->where('id', "=", $id);
        return $query;
    }

    public function scopeCustomerName($query, $customer_name) {
        if($customer_name)
            return $query->where('customer_name', 'LIKE', "%{$customer_name}%");
        return $query;
    }

    public function scopeCustomerEmail($query, $customer_email) {
        if($customer_email)
            return $query->where('customer_email', 'LIKE', "%{$customer_email}%");
        return $query;
    }

    public function scopeOrderDate($query, $order_date) {
        if($order_date)
            return $query->where('order_date', "=", $order_date);
        return $query;
    }

    public function scopeNetTotal($query, $net_total) {
        if($net_total)
            return $query->where('net_total', "=", $net_total);
        return $query;
    }

    public function scopeOrderStatus($query, $order_status) {
        if($order_status)
            return $query->where('order_status', "LIKE", "%{$order_status}%");
        return $query;
    }

    public function scopeCreatedAt($query, $created_at) {
        if($created_at)
            return $query->where('created_at', "=", $created_at);
        return $query;
    }

    public function scopeUpdatedAt($query, $updated_at) {
        if($updated_at)
            return $query->where('updated_at', "=", $updated_at);
        return $query;
    }

    public function scopeOrder($query, $orderBy)
    {
        if($orderBy) {
            $columns = ["id", "customer_name", "customer_email", "order_date", "net_total", "order_status", "created_at", "updated_at"];
            $query->orderBy($columns[$orderBy[0]['column']], $orderBy[0]['dir']);
        }
        return $query;
    }

    public function scopeLimitBy($query, $start = 0, $length = 10)
    {
        if($length == -1)
            return $query;
        return $query->offset($start)->limit($length);
    }
}
