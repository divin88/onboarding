<?php

namespace App\Features\Orders\Domain\Models;

use App\Features\Orders\Domain\Models\Constants\OrderConstants;
use App\Features\Orders\Domain\Models\Constants\OrderItemConstants;
use App\Features\Orders\Http\Controllers\Admin\V1\Actions\OrderItemsAction;
use App\Helpers\Models\BaseModel;
use App\Helpers\Services\Utils;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\DB;

class OrderItem extends BaseModel implements OrderItemConstants
{
    protected $table = "order_items";

    public function order():BelongsTo
    {
        return $this->belongsTo(Order::class);
    }
    public static function persistOrderItem(array $data): self
    {
        Utils::validateOrThrow($data, self::persistRules());
        return DB::transaction(function () use ($data) {
            return OrderItem::create($data);
        });
    }
    public static function persistRules(): array
    {
        return self::PERSIST_VALIDATION_RULES;
    }

    public static function updateRules(): array
    {
        return self::UPDATED_VALIDATION_RULES;
    }
}
