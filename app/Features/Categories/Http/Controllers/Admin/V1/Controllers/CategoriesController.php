<?php

namespace App\Features\Categories\Http\Controllers\Admin\V1\Controllers;

use App\DataTables\CategoriesDataTable;
use App\Features\Categories\Domain\Exports\CategoriesExport;
use App\Features\Categories\Domain\Models\Category;
use App\Features\Categories\Http\Controllers\Admin\V1\Actions\CategoriesAction;
use App\Features\Products\Http\Controllers\Admin\V1\Actions\ProductsAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\CreateCategoryWithProductsRequest;
use App\Http\Requests\UpdateCategoryRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class CategoriesController extends Controller
{
    public function __construct(private CategoriesAction $categoriesAction, private ProductsAction $productsAction) {}
    public function index(CategoriesDataTable $dataTable)
    {
        return $dataTable->render("categories.index");
    }

//    public function create()
//    {
//        return view("categories.create");
//    }
//
//    public function store(CreateCategoryRequest $request)
//    {
////        dd($request);
//        try {
//            $count = count($request->name);
//            for ($i=0; $i < $count; $i++) {
//                $data["name"] = $request->name[$i];
//                $data["description"] = $request->description[$i];
//                $data["is_active"] = $request->is_active[$i];
//                $this->categoriesAction->persistCategory($data);
//            }
//            session()->flash("success", "Category created successfully!");
//        } catch (\Exception $exception) {
//            Log::info($exception);
//            session()->flash("error", "Error while creating!");
//        }
//        return redirect()->route("categories.index");
//    }

    public function edit(Category $category)
    {
        return view("categories.edit", compact(["category"]));
    }

    public function update(Category $category, UpdateCategoryRequest $request)
    {
        try {
            $data["name"] = $request->name;
            $data["description"] = $request->description;
            $data["is_active"] = $request->is_active === "1";
            $this->categoriesAction->updateCategory($category, $data);
            session()->flash("success", "Category details updated successfully!!");
        } catch (\Exception $exception) {
            Log::info($exception);
            session()->flash("error", "Error while updating!");
        }
        return redirect()->back();
    }

//    public function destroy(Category $category)
//    {
//        $category->delete();
//
//        session()->flash('success', 'Category deleted successfully...');
//        return redirect()->route('categories.index');
//    }

    public function create()
    {
        $categories = Category::all();
        return view("categories.create", compact(['categories']));
    }

    public function store(Request $request)
    {
        try {
            $categoryData["name"] = $request->name;
            $categoryData["description"] = $request->description;
            $categoryData["is_active"] = $request->is_active === "1";
            $category = $this->categoriesAction->persistCategory($categoryData);

            if(!$request->get("outer_group")) {
                session()->flash('success', 'Category created successfully...');
                return null;
            }
            $result = $request["outer_group"][0]["inner_group"];

            $count = count($result);
//            dd($count, $result);
            for ($i=0; $i < $count; $i++) {
                $productData["category_id"] = $category->id;
                $productData["name"] = $result[$i]["product_name"];
                $productData["description"] = $result[$i]["product_description"];
                $productData["price"] = $result[$i]["product_price"];
                $productData["stock"] = $result[$i]["product_stock"];
                $productData["is_active"] = $result[$i]["product_is_active"];
//                dd($productData);
                $this->productsAction->persistProduct($productData);
            }
            session()->flash('success', 'Category and product created successfully...');
        }catch (\Exception $exception){
            Log::info($exception);
            session()->flash("error", "Error while creating!");
        }
        return redirect()->route('categories.index');
    }

    public function export()
    {
        return Excel::download(new CategoriesExport, 'categories.xlsx');
    }

    public function import(Request $request)
    {
        $file = $request->file('file');
        $storedFile = $file->storeAs(
        "temp/import-files",
        "temp".uniqid().".".$file->getClientOriginalExtension()
        );

        if(!$file) {
            Log::error("File not found");
            return redirect(route("categories.index"));
        }
        $this->categoriesAction->importCategories($storedFile);
        session()->flash('success', "Imported Successfully.");
        return redirect(route("categories.index"))
            ->with("success", "Import has begun successfully, will be completed in some time!");
    }
}
