<?php

namespace App\Features\Categories\Http\Controllers\Admin\V1\Actions;

use App\Features\Categories\Domain\Imports\CategoriesImport;
use App\Features\Categories\Domain\Models\Category;
use App\Features\Products\Http\Controllers\Admin\V1\Actions\ProductsAction;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\CreateProductRequest;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CategoriesAction
{
    public function persistCategories(CreateCategoryRequest $request)
    {
        $count = count($request->name);
        for ($i=0; $i < $count; $i++) {
            $data["name"] = $request->name[$i];
            $data["description"] = $request->description[$i];
            $data["is_active"] = $request->is_active[$i];
            Category::persistCategory($data);
        }
    }
    public function persistCategory(array $data): Category
    {
            return Category::persistCategory($data);
    }

    public function updateCategory(Category $category, array $data):Category
    {
        return $category->updateCategory($data);
    }

    public function importCategories(string $storedFile): void
    {
        Excel::queueImport(new CategoriesImport, $storedFile, 'public')->onQueue("default");
    }

}
