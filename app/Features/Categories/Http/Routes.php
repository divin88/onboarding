<?php

use Illuminate\Support\Facades\Route;

Route::resource(
    '/categories', \App\Features\Categories\Http\Controllers\Admin\V1\Controllers\CategoriesController::class
)->except('show');
Route::get(
    "categories/create/products",
    [\App\Features\Categories\Http\Controllers\Admin\V1\Controllers\CategoriesController::class, 'createCategoryAndProducts']
)->name("categories.createCategoryAndProducts");

Route::post(
    "categories/create/products",
    [\App\Features\Categories\Http\Controllers\Admin\V1\Controllers\CategoriesController::class, 'storeCategoryAndProducts']
)->name("categories.storeCategoryAndProducts");

Route::get('categories/export', [\App\Features\Categories\Http\Controllers\Admin\V1\Controllers\CategoriesController::class, 'export'])->name("categories.export");

Route::post('categories/import', [\App\Features\Categories\Http\Controllers\Admin\V1\Controllers\CategoriesController::class, 'import'])->name("categories.import");

Route::post(
    "/users/get-products",
    [\App\Features\Products\Http\Controllers\Admin\V1\Controllers\ProductsController::class, 'getProducts']
)->name("products.get-products");
