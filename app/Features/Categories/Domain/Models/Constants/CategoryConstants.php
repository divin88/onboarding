<?php

namespace App\Features\Categories\Domain\Models\Constants;

interface CategoryConstants
{
    public const PERSIST_VALIDATION_RULES = [
        "name" => "required|max:255|string",
        "description" => "nullable|string",
        "is_active" => "required|boolean"
    ];

    public const UPDATE_VALIDATION_RULES = [
        "name" => "required|max:255|string",
        "description" => "nullable|string",
        "is_active" => "required|boolean"
    ];
}
