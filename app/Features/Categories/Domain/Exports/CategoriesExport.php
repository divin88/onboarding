<?php

namespace App\Features\Categories\Domain\Exports;

use App\Features\Categories\Domain\Models\Category;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CategoriesExport implements FromCollection, WithHeadings,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Category::all();
    }

    public function headings(): array
    {
        return [
            "ID",
            "Name",
            "Description",
            "Is Active",
            "Created At",
            "Updated At"
        ];
    }
}
