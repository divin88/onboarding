<?php

namespace App\Features\Categories\Domain\Exports;

use App\Features\Categories\Domain\Models\Category;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class CategorySampleExport implements FromQuery, WithTitle, WithHeadings
{
    public function query()
    {
        return Category::query()->select("id", "name");
    }

    public function headings(): array
    {
        return [
            "ID",
            "Name",
        ];
    }

    public function title(): string
    {
        return "Categories";
    }
}
