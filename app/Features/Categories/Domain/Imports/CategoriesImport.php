<?php

namespace App\Features\Categories\Domain\Imports;

use App\Features\Categories\Domain\Models\Category;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CategoriesImport implements ToModel, WithHeadingRow,ShouldQueue, WithChunkReading
{
    protected $rules = [
        "name" => "required|max:255|string",
        "description" => "nullable|string",
        "is_active" => "required|boolean"
    ];
    public function makeData(array $row)
    {
        return [
            "name" => trim($row["name"]),
            "description" => trim($row["description"]),
            "is_active" => trim($row["is_active"]) == "" ? false : trim($row["is_active"]),
        ];
    }

    public function model(array $row)
    {
        $data = $this->makeData($row);
        $validator = Validator::make($data, $this->rules);
        if ($validator->fails()) {
            Log::error("Failed to import categories with name :{$data['name']}");
            return null;
        }
        return new Category($data);
    }

    public function chunkSize(): int
    {
        return 10;
    }
}
