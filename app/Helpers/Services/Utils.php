<?php

namespace App\Helpers\Services;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class Utils
{
    public static function validateOrThrow(array $data, array $validationRules, array $calidationRulesMessages = []): array {
        $validator = Validator::make($data, $validationRules, $calidationRulesMessages);

        if ($validator->fails()) {
            throw ValidationException::withMessages($validator->getMessageBag()->toArray());
        }
        return $validator->validated();
    }
}
