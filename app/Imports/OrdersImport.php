<?php

namespace App\Imports;

use App\Features\Orders\Domain\Models\Order;
use App\Features\Products\Domain\Models\Product;
use App\Rules\CheckStock;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class OrdersImport implements ToCollection, WithHeadingRow
{
    protected array $errorBag;
    protected array $orderData;
    protected int $orderId;
    protected Order $order;

    public function __construct(array $orderData, $orderId)
    {
        $this->orderData = $orderData;
        $this->orderId = $orderId;
    }

    public function makeData(Collection $row, $orderId)
    {
        try{
            $price = Product::get()->where('id', '=', $row['product_id'])->pluck('price');
            return [
                "order_id" => $orderId,
                "product_id" => trim($row["product_id"]),
                "quantity" => trim($row["quantity"]),
                "unit_price" => $price[0],
            ];
        } catch (\Exception $exception) {
            Log::error($exception);
            return null;
        }
    }

    public function collection(Collection $collection): void
    {
        $netTotal = 0;
        $order = Order::findOrFail($this->orderId);
        foreach ($collection as $row){
            $rowData = $this->makeData($row, $this->orderId);

            $rules = [
                "order_id" => "required|exists:orders,id",
                "product_id" => "required|exists:products,id",
                "unit_price" => "required|min:1",
                "quantity" => ["required", "min:1", new CheckStock($rowData['product_id'])]
            ];

            if ($rowData === null) {
                $this->errorBag[] = [
                    $this->orderData['customer_name'],
                    $this->orderData['customer_email'],
                    $this->orderData['order_date'],
                    $row['product_id'],
                    $row['quantity'],
                    "FAIL",
                    "Product not found"
                ];
                continue;
            }

            $validator = Validator::make($rowData, $rules);
            if ($validator->fails()) {
                Log::error("Failed to import order with name: {$this->orderData['customer_name']} and category id: {$rowData['product_id']}");
                $errors = implode(', ', $validator->errors()->all());
                $this->errorBag[] = [
                    $this->orderData['customer_name'],
                    $this->orderData['customer_email'],
                    $this->orderData['order_date'],
                    $rowData['product_id'],
                    $rowData['quantity'],
                    "FAIL",
                    $errors
                ];
            } else {
                $product = Product::findOrFail($rowData['product_id']);
                Log::info("Imported product with id: {$rowData['product_id']}");
                $this->errorBag[] = [
                    $this->orderData['customer_name'],
                    $this->orderData['customer_email'],
                    $this->orderData['order_date'],
                    $rowData['product_id'],
                    $rowData['quantity'],
                    "PASS",
                    "-"
                ];

                $totalPrice = ($rowData['unit_price'] * $rowData['quantity']);

                $order->orderItems()->create([
                    'product_id' => $rowData['product_id'],
                    'quantity' => $rowData['quantity'],
                    'unit_price' => $rowData['unit_price']
                ]);
                $netTotal += $totalPrice;
                $product->decrement('stock', $rowData['quantity']);
            }
        }
        $order->update(['net_total' => $netTotal]);
    }

    public function getErrorBag(): array
    {
        return $this->errorBag;
    }
}
