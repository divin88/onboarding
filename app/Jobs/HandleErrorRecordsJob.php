<?php

namespace App\Jobs;

use App\Features\Products\Domain\Exports\ErrorRecordsExport;
use App\Features\Products\Domain\Imports\ProductsImport;use App\Mail\ErrorRecords;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class HandleErrorRecordsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected array $errorBag;
    protected string $storedFile;
    public function __construct(string $storedFile)
    {
        $this->storedFile = $storedFile;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $import = new ProductsImport();
        Excel::import($import, $this->storedFile);
        $errors = $import->getErrorBag();

        $errorFilePath = 'response_' . now()->format('Y-m-d_H-i-s') . '.xlsx';
        $errorExport = new ErrorRecordsExport($errors);
        Excel::store($errorExport, $errorFilePath, "public");
        Log::info('Response is stored is ' . $errorFilePath);

        Mail::to("divij.ningu2026@onestopengineering.in")->send(new ErrorRecords($errorFilePath));

    }
}
