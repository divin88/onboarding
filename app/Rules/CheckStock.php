<?php

namespace App\Rules;

use App\Features\Products\Domain\Models\Product;
use Illuminate\Contracts\Validation\Rule;

class CheckStock implements Rule
{
    protected $productId;

    public function __construct($productId)
    {
        $this->productId = $productId;
    }

    public function passes($attribute, $value)
    {
        $product = Product::findOrFail($this->productId);
        return $product->stock;
    }

    public function message()
    {
        return 'The quantity must not exceed the product stock.';
    }
}
