<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
//        dd(request());
        return [
            "name.*" => "required|min:2|max:255|string",
            "category_id.*" => "required|exists:categories,id",
            "description.*" => "nullable|min:3|max:1023|string",
            "price.*" => "required|numeric|min:1",
            "stock.*" => "required|integer",
            "is_active.*" => "required|boolean",
        ];
    }
}
