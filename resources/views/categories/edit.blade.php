@extends('layouts.app')
@section('main-content')
    <div class="container mt-auto">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Update Category</h3>
                    </div>
                    <div class="card-body">
                        <form action="{{route("categories.update", $category)}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="categoryName">Name</label>
                                <input type="text" class="form-control mt-1" name="name" placeholder="Category" value="{{ $category->name }}">
                            </div>
                            <div class="form-group mt-3">
                                <label for="categoryDescription">Description</label>
                                <textarea class="form-control mt-1" name="description" style="resize: none;" rows="3" placeholder="Some short description of category">{{$category->description}}
                                </textarea>
                            </div>
                            <div class="form-group mt-3">
                                <label for="categoryStatus">Status</label>
                                <select type="text" class="form-control mt-1" name="is_active">
                                    <option value="1" {{ $category->is_active ? 'selected' : ''}}>Active</option>
                                    <option value="0" {{ !$category->is_active ? 'selected' : ''}}>Inactive</option>
                                </select>
                            </div>
                            <button type="submit" value="Submit" class="btn btn-primary mt-3">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

