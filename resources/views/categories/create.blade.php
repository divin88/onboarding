@extends('layouts.app')
@section('main-content')

<div class="container mt-auto">
    <div class="row justify-content-center">
        <div class="col-md-6">
            {{--                <form action="{{route("products.store")}}" method="POST">--}}
            {{--                    @csrf--}}
            {{--                    --}}
            {{--                </form>--}}
            <div class="card">
                <form action="{{route("categories.store")}}" method="POST" id="form-id">
                    @csrf
                    <div class="card-header">
                        <h2 class="card-title fw-bolder">Create Category</h2>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="category-name">Name</label>
                            <input type="text" class="validate_name form-control mt-1" name="name" placeholder="Category">
                        </div>
                        <div class="form-group mt-3">
                            <label for="category-description">Description</label>
                            <textarea class="form-control mt-1" style="resize: none;" name="description" rows="3" placeholder="Some short description of category"></textarea>
                        </div>
                        <div class="form-group mt-3">
                            <label for="category-status">Status</label>
                            <select type="text" class="form-control form-select mt-1" name="is_active">
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                        <div class="outer-repeater">
                            <div data-repeater-list="outer_group" class="outer">
                            <div data-repeater-item class="outer">
                                <!-- <input type="text" name="text-input" value="A" class="form-control outer"/> -->
                                <div class="inner-repeater">
                                    <div data-repeater-list="inner_group" class="inner">
                                        <div data-repeater-item class="inner">

                                            <div id="parent-form">
                                                <div class="child">
                                                    <div class="card-header">
                                                        <h2 class="card-title fw-bolder">Create Product</h2>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="form-group row">
                                                            <div class="col-md-6">
                                                                <label for="name">Name</label>
                                                                <input class="validate_name form-control mt-1" name="product_name" placeholder="iPhone 15">
                                                            </div>
                                                        </div>
                                                        <div class="form-group mt-3">
                                                            <label for="product-description">Description</label>
                                                            <textarea class="validate_description form-control mt-1" style="resize: none;" name="product_description" placeholder="Some short description of category"></textarea>
                                                        </div>
                                                        <div class="form-group row mt-3">
                                                            <div class="col-md-6">
                                                                <label for="product-price">Price</label>
                                                                <input class="validate_price form-control mt-1" name="product_price" placeholder="₹0.00">
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label for="product-stock">Stock</label>
                                                                <input class="validate_stock form-control mt-1" name="product_stock" placeholder="0">
                                                            </div>
                                                        </div>
                                                        <div class="form-group mt-3">
                                                            <label for="status">Status</label>
                                                            <select type="text" class="form-control form-select mt-1" name="product_is_active">
                                                                <option value="1">Active</option>
                                                                <option value="0">Inactive</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                            <input data-repeater-delete type="button" value="Delete" class="btn btn-danger inner ms-4 mb-3"/>
                                </div>
                                {{--                                <input data-repeater-create type="button" value="Add" class="btn btn-warning inner"/>--}}
                            </div>
                        </div>
                            <div data-repeater-create id="add-more-btn" class=" ms-4 btn btn-light btn-outline-dark btn-sm p-2 mt-3 mb-2 fw-bolder">+ Add more</div>
                            <div class="card-footer">
                                <button type="submit" value="Submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script src="{{asset("vendor/jquery/jquery.js")}}"></script>
    <script src="{{asset("jquery-validation/dist/jquery.validate.js")}}"></script>
    <script src="{{asset("jquery-validation/dist/additional-methods.js")}}"></script>
    <script src="{{asset("vendor/jquery/jquery.repeater.min.js")}}"></script>

    <script>
        $(document).ready(function () {
            'use strict';

            $('.repeater').repeater({
                defaultValues: {
                    'textarea-input': 'foo',
                    'text-input': 'bar',
                    'select-input': 'B',
                    'checkbox-input': ['A', 'B'],
                    'radio-input': 'B'
                },
                show: function () {
                    $(this).slideDown();
                },
                hide: function (deleteElement) {
                    if(confirm('Are you sure you want to delete this element?')) {
                        $(this).slideUp(deleteElement);
                    }
                },
                ready: function (setIndexes) {

                }
            });

            window.outerRepeater = $('.outer-repeater').repeater({
                initEmpty:true,
                isFirstItemUndeletable: true,
                // defaultValues: { 'text-input': 'outer-default' },
                show: function () {
                    console.log('outer show');
                    $(this).slideDown();
                },
                hide: function (deleteElement) {
                    console.log('outer delete');
                    $(this).slideUp(deleteElement);
                },
                repeaters: [{
                    isFirstItemUndeletable: true,
                    selector: '.inner-repeater',
                    // defaultValues: { 'inner-text-input': 'inner-default' },
                    show: function () {
                        console.log('inner show');
                        $(this).slideDown();
                    },
                    hide: function (deleteElement) {
                        console.log('inner delete');
                        $(this).slideUp(deleteElement);
                    }
                }]
            });
        });
    </script>

    <script>
        jQuery.validator.addClassRules({
            validate_category_id: {
                required: true
            },
            validate_name: {
                required:true,
                minlength:2,
                maxlength:255
            },
            validate_description: {
                maxlength:1023
            },
            validate_price: {
                required:true,
                digits: true
            },
            validate_stock: {
                required:true,
                digits: true,
                minlength: 1
            }
        });
        $('#form-id').validate();
    </script>
@endsection
