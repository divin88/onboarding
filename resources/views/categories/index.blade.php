@extends("layouts.app")
@section('main-content')
    <div class="container">
        <div class="d-sm-flex align-items-center justify-content-end mt-3 mb-4">
            <div class="d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center justify-content-between bg-white py-2 px-4 mr-2 border">
                    <form action="{{ route("categories.import") }}" enctype="multipart/form-data" method="POST">
                        @method("POST")
                        @csrf
                        <input type = "file" name="file">
                        <button type="submit" class="btn btn-info">Import Excel</button>
                    </form>
                </div>
                <a href="{{route("categories.export")}}" class="ms-2 me-2 btn btn-success">Export Excel</a>
            </div>

        </div>

        <div class="card d-flex shadow mb-4">
            <div class="card-header py-3">
                <h1 class="m-0 text-primary fw-bolder">Categories</h1>
                <div class="col-auto ms-auto d-print-non">
                    <div class="btn-list">
                        <a href="{{route("categories.create")}}" class="btn btn-info">+ Create New Category</a>
                    </div>

                </div>
            </div>
            <div class="card-body">
                {{ $dataTable->table() }}
            </div>
        </div>
    </div>


@endsection
@section("styles")
    <link href="{{asset("vendor/datatables/dataTables.bootstrap4.min.css")}}" rel="stylesheet">
@endsection
@section("scripts")
    <script src="{{ asset("vendor/jquery/jquery.min.js")}}"></script>
    <script src="{{ asset("vendor/datatables/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("vendor/datatables/dataTables.bootstrap4.min.js") }}"></script>

    {{ $dataTable->scripts() }}
@endsection
