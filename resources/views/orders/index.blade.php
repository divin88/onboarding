@extends("layouts.app")

@section('main-content')
    <div class="container">
        <div class="d-sm-flex align-items-center justify-content-end mt-3 mb-4">
            <div class="d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center justify-content-between mr-2">
                    <button data-bs-toggle="modal" data-bs-target="#importModal" class="btn btn-info">Import Excel<svg  xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline icon-tabler-file-spreadsheet ms-1"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M14 3v4a1 1 0 0 0 1 1h4" /><path d="M17 21h-10a2 2 0 0 1 -2 -2v-14a2 2 0 0 1 2 -2h7l5 5v11a2 2 0 0 1 -2 2z" /><path d="M8 11h8v7h-8z" /><path d="M8 15h8" /><path d="M11 11v7" /></svg></button>
                </div>
                <form action="{{route("orders.export")}}" method="POST" enctype="multipart/form-data">
                    @method("POST")
                    @csrf
                    <input hidden type="text" name="form_order_id" id="form_order_id"/>
                    <button type="submit" id="export-btn" class="ms-2 me-2 btn btn-success">Export Excel<svg  xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline ms-1 icon-tabler-file-spreadsheet"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M14 3v4a1 1 0 0 0 1 1h4" /><path d="M17 21h-10a2 2 0 0 1 -2 -2v-14a2 2 0 0 1 2 -2h7l5 5v11a2 2 0 0 1 -2 2z" /><path d="M8 11h8v7h-8z" /><path d="M8 15h8" /><path d="M11 11v7" /></svg></button>
                </form>
            </div>
        </div>
        <div class="card d-flex shadow mb-4">
            <div class="card-header py-3">
                <h1 class="m-0 text-primary fw-bolder">Orders</h1>
                <div class="col-auto ms-auto d-print-non">
                    <div class="btn-list">
                        <a href="{{route("orders.create")}}" class="btn btn-info">+ Place new order</a>
                        <a href="#" class="btn btn-yellow" data-bs-toggle="modal" data-bs-target="#modal-filter"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round" class="icon icon-tabler icons-tabler-outline icon-tabler-filter"><path stroke="none" d="M0 0h24v24H0z" fill="none" /><path d="M4 4h16v2.172a2 2 0 0 1 -.586 1.414l-4.414 4.414v7l-6 2v-8.5l-4.48 -4.928a2 2 0 0 1 -.52 -1.345v-2.227z" /></svg>Filter</a>
                    </div>

                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable">
                        <thead>
                            <tr>
                                <th>Order Id</th>
                                <th>Customer Name</th>
                                <th>Customer Email</th>
                                <th>Order Date</th>
                                <th>Net Total</th>
                                <th>Order Status</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-filter" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel"
         aria-hidden="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="filterModalLabel">Filter Orders</h5>
                    <button class="close btn-close delete-url" type="button" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div>
                        <label for="order-id" class="form-label mb-0">Order Id</label>
                        <input type="text" id="order_id" name="order_id" class="form-control mb-2" placeholder="Search order id" aria-label="">
                    </div>
                    <div>
                        <label for="customer-name" class="form-label">Customer Name</label>
                        <input type="text" id="customer_name" name="customer_name" class="form-control mb-2" placeholder="name you want to search" aria-label="">
                    </div>
                    <div>
                        <label for="customer-email" class="form-label">Customer Email</label>
                        <input type="email" id="customer_email" name="customer_email" class="form-control mb-2" placeholder="email you want to search" aria-label="">
                    </div>

                    <div>
                        <label for="net_total" class="form-label">Net Total</label>
                        <input type="text" id="net_total" value="" name="net_total" class="form-control mb-2 col-6" placeholder="Search net total">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="datepicker-icon" class="form-label">Order Date</label>
                            <input class="date form-control" placeholder="Select date" id="datepicker-icon"/>
                        </div>
                        <div class="col-md-6">
                            <label for="status" class="form-label">Order Status</label>
                            <select type="text" class="form-control form-select mb-2" name="order_status" id="order_status">
                                <option></option>
                                <option value="Pending">Pending</option>
                                <option value="Completed">Completed</option>
                                <option value="Cancelled">Cancelled</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label">Created At</label>
                                <div class="input-group input-group-flat">
                                    <input type="text" name="created_at" id="created_at" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label">Updated At</label>
                                <input type="text" name="updated_at" id="updated_at" class="form-control">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger delete-url" type="button" id="reset_btn" data-bs-dismiss="modal">Reset Filter</button>
                    <button class="btn btn-primary" data-bs-dismiss="modal" aria-label="Close" id="filter_btn">Filter</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="importModalLabel"
         aria-hidden="false">
        <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="importModalLabel">Import Products?</h5>
                        <button class="close delete-url" type="button" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form method="POST" id="import_model" action="{{ route("orders.import") }}" enctype="multipart/form-data" id="importForm">
                        @csrf
                        <div class="modal-body">
                            <div class="mb-3">
                                <label class="form-label">Customer Name</label>
                                <input type="text" class="validate_name form-control" name="customer_name" placeholder="your name">
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Customer Email</label>
                                <input type="email" class="validate_email form-control" name="customer_email" placeholder="your email">
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Order Date</label>
                                <input type="date" class="validate_date form-control" name="order_date" placeholder="Select a date" value=""/>
                            </div>
                            <div>
                                <input class="validate_file" type="file" name="file">
                                <button type="submit" class="btn btn-info ms-5">Import Excel</button>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <form action="{{route("orders.sample")}}" method="POST"> @method("POST") @csrf
                            <input type="submit" class="me-5 btn btn-primary" value="Download Sample File"/>
                        </form>
                    </div>
                </div>
        </div>
    </div>

@endsection
@section("style")
    <link href="{{asset("vendor/datatables/dataTables.bootstrap4.min.css")}}" rel="stylesheet">
@endsection

@section("scripts")
    <script src="{{asset("vendor/jquery/jquery.min.js")}}"></script>
    <script src="{{ asset("vendor/datatables/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("vendor/datatables/dataTables.bootstrap4.min.js") }}"></script>

    <script>
        $(document).ready(function() {
            const order_ids = document.querySelector("#form_order_id");
            $('#dataTable').DataTable({
                serverSide: true,
                "searching": false,
                // searchBuilder: true,
                ajax: {
                    url: '{{route("orders.get-orders")}}',
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': ' {{ csrf_token() }}'
                    },
                    data:function(d) {
                        d.search.id = $("#order_id").val();
                        d.search.customer_name = $("#customer_name").val();
                        d.search.customer_email = $("#customer_email").val();
                        d.search.order_date = $(".date").val();
                        d.search.net_total = $("#net_total").val();
                        d.search.order_status = $("#order_status").val();
                        d.search.created_at = $('#created_at').val();
                        d.search.updated_at = $('#updated_at').val();
                    }
                },
                lengthMenu: [
                    [50,100,150,-1],
                    [50,100,150,"All"]
                ],
                order: [
                    [0, "DESC"]
                ],
            });
            $('#filter_btn').on('click', function () {
                $('#dataTable').DataTable().draw();
                setTimeout(() => {
                    order_ids.value = $('#dataTable').DataTable().column(0).data().toArray()
                    console.log($('#dataTable').DataTable().column(0).data().toArray());
                }, 3000)
            })

            $('#reset_btn').on('click', function () {
                $("#form_order_id").val(null)
                $("#order_id").val(null);
                $("#form_customer_name").val(null)
                $("#customer_name").val(null)
                $("#form_customer_email").val(null)
                $("#customer_email").val(null)
                $("#form_order_date").val(null)
                $(".date").val(null)
                $("#form_order_status").val(null)
                $("#order_status").val(null)
                $("#form_net_total").val(null)
                $("#net_total").val(null)
                $('#dataTable').DataTable().draw();
            })
        });
    </script>

    <script src="{{asset("litepicker/dist/litepicker.js")}}" ></script>

    <script src="{{asset("jquery-validation/dist/jquery.validate.js")}}"></script>
    <script src="{{asset("jquery-validation/dist/additional-methods.js")}}"></script>
    <script>
        document.addEventListener("DOMContentLoaded", function () {
            window.Litepicker && (new Litepicker({
                element: document.getElementById('datepicker-icon'),
                buttonText: {
                    previousMonth: `<!-- Download SVG icon from http://tabler-icons.io/i/chevron-left -->
    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M15 6l-6 6l6 6" /></svg>`,
                    nextMonth: `<!-- Download SVG icon from http://tabler-icons.io/i/chevron-right -->
    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M9 6l6 6l-6 6" /></svg>`,
                },
            }));
        });
    </script>
    <script>
        jQuery.validator.addClassRules({
            validate_name : {
                required:true,
                minlength:2,
                maxlength:50
            },
            validate_email: {
                required: true,
                email: true
            },
            validate_date : {
                required: true
            },
            validate_file: {
                required: true
            }
        });
        $('#import_model').validate();
    </script>

@endsection
