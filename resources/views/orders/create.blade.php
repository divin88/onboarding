@extends('layouts.app')
@section('main-content')

<div class="container mt-4">
    <div class="row justify-content-center">
        <div class="col-md-7">
            <div class="card">
                <form action="{{route("orders.store")}}" method="POST" id="form-id">
                    @csrf
                    <div class="card-header">
                        <h2 class="card-title fw-bolder">Place an Order</h2>
                    </div>
                    <div class="card-body row">
                        <div class="form-group">
                            <label for="customer-name">Customer Name</label>
                            <input type="text" class="validate_name form-control mt-1" name="customer_name" placeholder="your name" value="{{old('customer_name')}}">
                            @error('customer_name')
                            <span class="text-danger">
                                    {{$message}}
                                </span>
                            @enderror
                        </div>
                        <div class="form-group mt-3">
                            <label for="customer-email">Customer Email</label>
                            <input type="email" class="validate_email form-control mt-1" name="customer_email" placeholder="email@gmail.com" value="{{old('customer_email')}}">
                            @error('customer_email')
                            <span class="text-danger">
                                    {{$message}}
                                </span>
                            @enderror
                        </div>
                        <div class="form-group mt-3">
                            <label for="order-date">Order Date</label>
                            <div class="input-icon mt-1">
                                <input class="validate_date form-control" id="datepicker-icon" name="order_date" placeholder="your order date" value="{{old('order_date')}}">
                                @error('order_date')
                                <span class="text-danger">
                                    {{$message}}
                                </span>
                                @enderror
                                <span class="input-icon-addon">
                                <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M4 7a2 2 0 0 1 2 -2h12a2 2 0 0 1 2 2v12a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12z"></path><path d="M16 3v4"></path><path d="M8 3v4"></path><path d="M4 11h16"></path><path d="M11 15h1"></path><path d="M12 15v3"></path></svg>
                              </span>
                            </div>
                        </div>
                        <div class="outer-repeater">
                            <div data-repeater-list="orders" class="outer">
                                <div data-repeater-item class="outer">

                                    <div id="parent-form">
                                        <div class="child">
                                            <div class="mt-5">
                                                <h2 class="card-title fw-bolder">Order Items</h2>
                                            </div>
                                            <div class="">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="category-id" class="form-label">Select Category</label>
                                                            <select name="category_id" class="form-control select2-category">
                                                                <option selected="selected"></option>
                                                                @foreach ($categories as $category)
                                                                    <option value="{{$category->id}}" >{{$category->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="product-id" class="form-label">Select Product</label>
                                                            <select name="product_id" class="select2-product form-control">
                                                                <option></option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row mt-3">
                                                    <div class="col-md-6">
                                                        <label for="quantity">Quantity</label>
                                                        <input type="number" min=1 oninput="validity.valid||(value='');" class="validate_quantity quantity form-control mt-1" name="quantity" placeholder="1" data-stock="">
                                                        <span class="text-danger quantity-error"></span>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="unit-price">Unit price</label>
                                                        <input class="validate_unit_price form-control mt-1" name="unit_price" placeholder="₹0.00" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group mt-3">
                                                    <label for="total">Total price</label>
                                                    <input class="validate_total_price form-control mt-1" name="total_price" placeholder="₹0.00" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <input data-repeater-delete type="button" value="Delete" class="btn btn-danger inner mt-2"/>
                                </div>
                            </div>
                            <div class="form-group net mt-3">
                                <label for="net-total" class="fw-semibold label_net_total">Net Total: </label>
                                <input type="text" id="net-total" class="net_total form-control" name="net_total" readonly/>
                            </div>
                            <div data-repeater-create id="add-more-btn" class="btn btn-light btn-outline-dark btn-sm p-2 mt-3 fw-bolder">+ Add more</div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" value="Submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
{{--@section('styles')--}}
{{--    <script src="{{asset("css/select2.min.css")}}"></script>--}}
{{--@endsection--}}
@section("styles")
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('scripts')
    <script src="{{asset("vendor/jquery/jquery.js")}}"></script>
    <script src="{{asset("jquery-validation/dist/jquery.validate.js")}}"></script>
    <script src="{{asset("jquery-validation/dist/additional-methods.js")}}"></script>
    <script src="{{asset("vendor/jquery/jquery.repeater.min.js")}}"></script>
    <script src="{{asset("libs/litepicker/dist/litepicker.js")}}"></script>
{{--    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>--}}
        <script src="{{asset("js/select2.min.js")}}"></script>

    <script>
        jQuery.validator.addClassRules({
            validate_category_id: {
                required: true
            },
            validate_name: {
                required:true,
                minlength:2,
                maxlength:255
            },
            validate_email: {
                required: true,
                email: true
            },
            validate_date: {
                required: true,
                date: true
            },
            validate_quantity: {
                required:true,
                digits: true,
                min:1
            },
            validate_unit_price : {},
            validate_total_price : {}
        });
        $('#form-id').validate();
    </script>
    <script>
        $(document).ready(function () {
            $("#net-total").val(0);
            $('.select2-category').select2({
                placeholder: "Select a Category",
                allowClear: true
            });
            $('.select2-product').select2({
                placeholder: "Select a Product",
                allowClear: true
            });

            $(".select2-category").on("change", (evt) => {
                console.log(evt.target.value)
                loadProducts(evt);
            })

            $(".select2-product").on("change", (evt) => {
                console.log(evt.target.value)
                loadPriceAndQuantity(evt);
            })

            $(".quantity").on("change", (evt) => {
                // console.log(evt.target.value)
                updateQuantity(evt);
            })



            window.outerRepeater = $('.outer-repeater').repeater({
                // initEmpty:true,
                isFirstItemUndeletable: true,
                // defaultValues: { 'text-input': 'outer-default' },
                show: function () {
                    $('.select2-container').remove();

                    $(this).slideDown();

                    $('.select2-category').select2({
                        placeholder: "Select a Category",
                        allowClear: true
                    });
                    $('.select2-product').select2({
                        placeholder: "Select a Product",
                        allowClear: true
                    });

                    $('.select2-container').css('width', '100%');

                    $(".select2-category").on("change", (evt) => {
                        console.log(evt.target.value)
                        loadProducts(evt);
                    })

                    $(".select2-product").on("change", (evt) => {
                        // console.log(evt.target.value)
                        loadPriceAndQuantity(evt);
                    })

                    $(".quantity").on("change", (evt) => {
                        // console.log(evt.target.value)
                        updateQuantity(evt);
                    })
                },
                hide: function (deleteElement) {
                    // console.log(this.firstElementChild.firstElementChild.lastElementChild.lastElementChild.lastElementChild.value);
                    let deletedQuantityPrice = this.firstElementChild.firstElementChild.lastElementChild.lastElementChild.lastElementChild.value
                    let netValue = document.getElementById("net-total").value
                    document.getElementById("net-total").value = (parseFloat(netValue) - parseFloat(deletedQuantityPrice))

                    $(this).slideUp(deleteElement);
                },
            });
        });

        // $(".select2-category").select2({
        //     placeholder: "Select a value",
        //     allowClear: true
        // });



        function loadProducts(evt){
            console.log(evt.target)
            $.ajax({
                url: "{{route("orders.getProducts")}}",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN' : '{{ csrf_token() }}'
                },
                data: {
                    "category_id": evt.target.value
                }
            }).done(function (responses) {
                responses = JSON.parse(responses)
                let innerhtml = ''
                responses.forEach(function (responses) {
                    innerhtml = innerhtml + `<option selected="selected"></option>
                        <option class="select2-product" value="${responses.id}">${responses.name}</option>`
                })
                // console.log(innerhtml)
                // console.log(evt.target.parentElement.parentElement.nextElementSibling.lastElementChild.firstElementChild.nextElementSibling.innerHTML)
                evt.target.parentElement.parentElement.nextElementSibling.lastElementChild.firstElementChild.nextElementSibling.innerHTML = innerhtml
            })
        }
        function loadPriceAndQuantity(evt){
            // console.log(evt.target.value)
            $.ajax({
                url: "{{route("orders.getProductQuantityAndPrice")}}",
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN' : '{{ csrf_token() }}'
                },
                data: {
                    "product_id": evt.target.value
                }
            }).done(function (responses) {
                responses = JSON.parse(responses)
                let innerhtml = responses[0].price
                let stock = responses[0].stock;

                evt.target.parentElement.parentElement.parentElement.nextElementSibling.firstElementChild.firstElementChild.nextElementSibling.setAttribute("data-stock", stock)
                // console.log(evt.target.parentElement.parentElement.parentElement.nextElementSibling.firstElementChild.firstElementChild.nextElementSibling)
                evt.target.parentElement.parentElement.parentElement.nextElementSibling.lastElementChild.lastElementChild.value = parseFloat(innerhtml)
                evt.target.parentElement.parentElement.parentElement.nextElementSibling.firstElementChild.firstElementChild.nextElementSibling.value = 1
                evt.target.parentElement.parentElement.parentElement.parentElement.lastElementChild.lastElementChild.value = parseFloat(innerhtml)
                let netTotal = document.getElementById("net-total").value
                if(parseFloat(netTotal) === 0)
                    document.getElementById("net-total").value = parseFloat(innerhtml)
                else
                    document.getElementById("net-total").value = parseFloat(netTotal) +  parseFloat(innerhtml)
            })
        }

        function updateQuantity(evt){
            // let oldQuantity = 1
            let quantity = parseFloat(evt.target.value)
            let unit_price = parseFloat(evt.target.parentElement.nextElementSibling.lastElementChild.value)
            let total_price = evt.target.parentElement.parentElement.nextElementSibling.lastElementChild
            let stock = parseFloat(evt.target.dataset.stock)
            let quantity_error = evt.target.parentElement.lastElementChild
            let net_total = document.getElementById("net-total")
            console.log(stock)
            // let netTotal = 0

            if(quantity > 0 && quantity > stock) {
                // console.log(quantity_error);
                quantity_error.innerHTML = `Please enter quantity below ${stock}`
                net_total.value = (parseFloat(net_total.value) - parseFloat(total_price.value))
                total_price.value = 0
            }
            else {
                // if()
                let old_total_price = total_price.value
                quantity_error.innerHTML = ''
                total_price.value = (unit_price * quantity)
                getNetTotalPrice(old_total_price, total_price)

            }

        }


        function getNetTotalPrice(old_total_price, total_price_input_box) {
            let net_total = document.getElementById("net-total")
            console.log(net_total.value)
            console.log(old_total_price, total_price_input_box.value)

            let totalPriceOfProduct = parseFloat(total_price_input_box.value)
            console.log(typeof(totalPriceOfProduct))

            if(!totalPriceOfProduct === NaN)
               totalPriceOfProduct = 0

            // total_price.value
            if(!net_total.value){
                console.log("if")
                net_total.value = totalPriceOfProduct
            }else {
                if(parseFloat(old_total_price) > parseFloat(totalPriceOfProduct)) {
                    console.log("old is greater")
                    net_total.value = parseFloat(net_total.value) - old_total_price
                    net_total.value = parseFloat(net_total.value) + totalPriceOfProduct
                } else if (old_total_price < totalPriceOfProduct) {
                    console.log("total is greater")
                    net_total.value = parseFloat(net_total.value) - old_total_price
                    net_total.value = parseFloat(net_total.value) + totalPriceOfProduct
                }
                // netTotal = parseFloat(net_total.value)
                // netTotal = parseFloat(netTotal) - parseFloat(totalPrice)
                // // if()
                // net_total.value = parseFloat(netTotal)
                // total_price.value = (unit_price * quantity)
                // net_total.value = parseFloat(net_total.value) +  parseFloat(total_price.value)
            }
        }


    </script>

    <script>
        // @formatter:off
        document.addEventListener("DOMContentLoaded", function () {
            window.Litepicker && (new Litepicker({
                element: document.getElementById('datepicker-icon'),
                buttonText: {
                    previousMonth: `<svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M15 6l-6 6l6 6" /></svg>`,
                    nextMonth: `<svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M9 6l6 6l-6 6" /></svg>`,
                },
            }));
        });

    </script>
@endsection
