@extends('layouts.app');
@section('main-content')
    <div class="page">
        <div class="page-wrapper">
            <div class="container-xl">
                <div class="page-header d-print-none">
                    <div class="row align-items-center">
                        <div class="col">
                            <h2 class="page-title">
                                Order Details
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="page-body">
                    <div class="container-xl">
                        <div class="row row-cards">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Order #{{ $order->id }}</h3>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-striped">
                                            <tbody>
                                            <tr>
                                                <th>Order ID</th>
                                                <td>{{ $order->id }}</td>
                                            </tr>
                                            <tr>
                                                <th>Customer Name</th>
                                                <td>{{ $order->customer_name }}</td>
                                            </tr>
                                            <tr>
                                                <th>Customer Email</th>
                                                <td>{{ $order->customer_email }}</td>
                                            </tr>
                                            <tr>
                                                <th>Order Date</th>
                                                <td>{{ $order->order_date }}</td>
                                            </tr>
                                            <tr>
                                                <th>Status</th>
                                                <td>{{ $order->order_status }}</td>
                                            </tr>
                                            <tr>
                                                <th>Net Amount</th>
                                                <td>{{ $order->net_total }}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Order Items</h3>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Order #</th>
                                                <th>Product #</th>
                                                <th>Quantity</th>
                                                <th>Price per unit</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($order->orderItems as $item)
                                                <tr>
                                                    <td>{{ $item->id }}</td>
                                                    <td>{{ $item->order_id }}</td>
                                                    <td>{{ $item->product_id }}</td>
                                                    <td>{{ $item->quantity }}</td>
                                                    <td>{{ $item->unit_price }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
