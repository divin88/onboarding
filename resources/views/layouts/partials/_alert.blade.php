<div class="container mt-2">
    @if(session("success"))
        <div class="alert alert-success alert-dismissible" role="alert"><svg xmlns="http://www.w3.org/2000/svg" class="icon alert-icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M5 12l5 5l10 -10"></path></svg><a class="btn-close" data-bs-dismiss="alert" aria-label="close"></a>
            {{ session("success") }}
        </div>
    @endif

    @if(session("error"))
        <div class="alert alert-danger alert-dismissible" role="alert"><svg xmlns="http://www.w3.org/2000/svg" class="icon alert-icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M5 12l5 5l10 -10"></path></svg><a class="btn-close" data-bs-dismiss="alert" aria-label="close"></a>
            {{ session("error") }}
        </div>
    @endif
</div>
