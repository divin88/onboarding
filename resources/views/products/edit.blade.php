@extends('layouts.app')
@section('main-content')
    <div class="container mt-auto">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Update Product</h3>
                    </div>
                    <div class="card-body">
                        <form action="{{route("products.update", $product)}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="category-name">Category</label>
                                    <select type="text" class="form-control mt-1 form-select" name="category_name">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}" {{$category->id === $product->category_id ? 'selected' : ''}}>{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label for="name">Name</label>
                                    <input class="form-control mt-1" style="resize: none;" name="name" placeholder="iPhone 15" value="{{$product->name}}">
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <label for="product-description">Description</label>
                                <textarea class="form-control mt-1" style="resize: none;" name="description" placeholder="Some short description of category">{{$product->description}}</textarea>
                            </div>
                            <div class="form-group row mt-3">
                                <div class="col-md-6">
                                    <label for="product-price">Price</label>
                                    <input class="form-control mt-1" style="resize: none;" name="price" placeholder="₹0.00" value="{{$product->price}}">
                                </div>
                                <div class="col-md-6">
                                    <label for="product-stock">Stock</label>
                                    <input class="form-control mt-1" style="resize: none;" name="stock" placeholder="0" value="{{$product->stock}}">
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <label for="status">Status</label>
                                <select type="text" class="form-control form-select mt-1" name="is_active">
                                    <option value="1" {{ $product->is_active ? 'selected' : ''}}>Active</option>
                                    <option value="0" {{ !$product->is_active ? 'selected' : ''}}>Inactive</option>
                                </select>
                            </div>
                            <button class="btn btn-light btn-outline-dark btn-sm p-2 mt-3 fw-bolder">+ Add more</button>
                            <div></div>
                            <button type="submit" value="Submit" class="btn btn-primary mt-3">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

