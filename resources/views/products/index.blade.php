@extends("layouts.app")
@section('main-content')
    <div class="container">
        <div class="d-sm-flex align-items-center justify-content-end mt-3 mb-4">
            <div class="d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center justify-content-between mr-2">
                        <button data-bs-toggle="modal" data-bs-target="#importModal" class="btn btn-info">Import Excel<svg  xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline icon-tabler-file-spreadsheet ms-1"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M14 3v4a1 1 0 0 0 1 1h4" /><path d="M17 21h-10a2 2 0 0 1 -2 -2v-14a2 2 0 0 1 2 -2h7l5 5v11a2 2 0 0 1 -2 2z" /><path d="M8 11h8v7h-8z" /><path d="M8 15h8" /><path d="M11 11v7" /></svg></button>
                </div>
                <form action="{{route("products.export")}}" method="GET">
                    @csrf
                    @method('GET')
                    <input type="hidden" name="hidden_name_input" id="hidden_search_name" placeholder="product name">
                    <input type="hidden" name="hidden_description_input" id="hidden_search_description"></input>
                    <input type="hidden" name="hidden_price_input" id="hidden_search_price">
                    <input type="hidden" name="hidden_stock_input" id="hidden_search_stock" class="form_control">
                    <select hidden name="hidden_is_active_input" class="form_select" id="hidden_search_is_active">
                        <option value="1" selected>Active</option>
                        <option value="0">Inactive</option>
                    </select>
                    <input type="hidden" name="hidden_created_at_input" id="hidden_search_created_at" class="form_control">
                    <input type="hidden" name="hidden_updated_at_input" id="hidden_search_updated_at" class="form_control">
                    <button type="submit" id="export-btn" class="ms-2 me-2 btn btn-success">Export Excel<svg  xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline ms-1 icon-tabler-file-spreadsheet"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M14 3v4a1 1 0 0 0 1 1h4" /><path d="M17 21h-10a2 2 0 0 1 -2 -2v-14a2 2 0 0 1 2 -2h7l5 5v11a2 2 0 0 1 -2 2z" /><path d="M8 11h8v7h-8z" /><path d="M8 15h8" /><path d="M11 11v7" /></svg></button>
                </form>
            </div>
        </div>


        <div class="card d-flex shadow mb-4">
            <div class="card-header py-3">
                <h1 class="m-0 text-primary fw-bolder">Products</h1>
                <div class="col-auto ms-auto d-print-non">
                    <div class="btn-list">
                        <a href="{{route("products.create")}}" class="btn btn-info">+ Create New Product</a>
                        <a href="#" class="btn btn-yellow" data-bs-toggle="modal" data-bs-target="#modal-report"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round" class="icon icon-tabler icons-tabler-outline icon-tabler-filter"><path stroke="none" d="M0 0h24v24H0z" fill="none" /><path d="M4 4h16v2.172a2 2 0 0 1 -.586 1.414l-4.414 4.414v7l-6 2v-8.5l-4.48 -4.928a2 2 0 0 1 -.52 -1.345v-2.227z" /></svg>Filter</a>
                    </div>

                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Category Id</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Stock</th>
                            <th>Is active</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="importModalLabel"
        aria-hidden="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route("products.import") }}" enctype="multipart/form-data" method="POST" id="importForm">
                    @csrf
                    @method('POST')

                        <div class="modal-header">
                            <h5 class="modal-title" id="importModalLabel">Import Excel</h5>
                            <button class="close btn-close" type="button" data-bs-dismiss="modal" aria-label="Close">
                            </button>
                        </div>
                        <div class="modal-body border p-0 py-2">
                            <input class="ms-5" type = "file" name="file">
                            <button type="submit" class="ms-5 btn btn-info">Import Excel</button>
                        </div>
                </form>
                <div class="modal-footer"><a href="{{route("products.downloadSample")}}" class="me-5 btn btn-primary"><svg  xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline icon-tabler-download"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M4 17v2a2 2 0 0 0 2 2h12a2 2 0 0 0 2 -2v-2" /><path d="M7 11l5 5l5 -5" /><path d="M12 4l0 12" /></svg> Sample File</a></div>
            </div>
        </div>
    </div>

    <div class="modal modal-blur fade" id="modal-report" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Filter Data</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <label class="form-label">Product Name</label>
                        <input type="text" class="form-control" name="name_input" id="search-name" placeholder="product name">
                    </div>
                    <div class="row">
                        <div class="col-lg-12 mb-3">
                            <div>
                                <label class="form-label">Product Description</label>
                                <textarea class="form-control" style="resize: none;" name="description_input" id="search-description" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-3">
                                <label class="form-label">Price</label>
                                <div class="input-group input-group-flat">
                                    <input type="text" name="price_input" id="search-price" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-3">
                                <label class="form-label">Stock</label>
                                <div class="input-group input-group-flat">
                                    <input type="text" name="stock_input" id="search-stock" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-3">
                                <label class="form-label">Status</label>
                                <select class="form-select" name="is_active_input" id="search-is-active">
                                    <option value="1" selected>Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div><div class="col-md-6"></div>
                        <div class="col-lg-6">
                            <div class="mb-3">
                                <label class="form-label">Created At</label>
                                <div class="input-group input-group-flat">
                                    <input type="text" name="created_at_input" id="search-created-at" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-3">
                                <label class="form-label">Updated At</label>
                                <input type="text" name="updated_at_input" id="search-updated-at" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
                        Cancel
                    </a>
                    <a href="#" class="btn btn-primary ms-auto" onclick="drawData()" data-bs-dismiss="modal">Filter</a>
                </div>
            </div>

        </div>
    </div>

@endsection
@section("styles")
    <link href="{{asset("vendor/datatables/dataTables.bootstrap4.min.css")}}" rel="stylesheet">

@endsection
@section("scripts")
    <script src="{{ asset("vendor/jquery/jquery.min.js")}}"></script>
    <script src="{{ asset("vendor/datatables/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("vendor/datatables/dataTables.bootstrap4.min.js") }}"></script>
    <script>
    $(document).ready(function() {
        $('#dataTable').DataTable({
            serverSide: true,
            ajax: {
                url: '{{ route("products.get-products") }}',
                type: 'POST',
                headers: {
                'X-CSRF-TOKEN': ' {{ csrf_token() }}'
                },
                data:function (d) {
                    d.search.name_input = $('#search-name').val();
                    d.search.description_input = $('#search-description').val();
                    d.search.price_input = $('#search-price').val();
                    d.search.stock_input = $('#search-stock').val();
                    d.search.is_active_input = $('#search-is-active').val();
                    d.search.created_at_input = $('#search-created-at').val();
                    d.search.updated_at_input = $('#search-updated-at').val();
                }
            },
            lengthMenu: [
                [50,100,150,-1],
                [50,100,150,"All"]
            ],
            order: [
                [0, "DESC"]
            ]
        });
    });
    </script>
    <script>
        // $("#export-btn").click(function () {
        //     // console.log("log");
        //
        //
        //
        // })


        function drawData(){
            console.log("here");
            // console.log(search.name_input)
            // $('#hidden_search_name').val() = $('#search-name').val()
            // document.querySelector("#hidden_search_name").setAttribute("value", document.querySelector('#search-name').getAttribute("value"))
            // console.log(document.querySelector("#hidden_search_name").getAttribute("value"))

            $('#hidden_search_name').val($('#search-name').val())
            $('#hidden_search_description').val($('#search-description').val())
            $('#hidden_search_price').val($('#search-price').val())
            $('#hidden_search_stock').val($('#search-stock').val())
            $('#hidden_search_is_active').val($('#search-is-active').val())
            $('#hidden_search_created_at').val($('#search-created-at').val())
            $('#hidden_search_updated_at').val($('#search-updated-at').val())

            $('#dataTable').DataTable().draw()
        }

    </script>
@endsection
