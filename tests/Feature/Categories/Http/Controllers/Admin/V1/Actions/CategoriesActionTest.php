<?php

namespace Categories\Http\Controllers\Admin\V1\Actions;

use App\Features\Categories\Domain\Models\Category;
use App\Features\Categories\Http\Controllers\Admin\V1\Actions\CategoriesAction;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class CategoriesActionTest extends TestCase
{
    use DatabaseMigrations;

    public function testUpdateMethodUpdateTheDetailsAndReturnTheCategoryInstance(){
        $categoryData = [
            "name" => "electronics",
            "description" => "devices that run on electricity",
            "is_active" => true,
        ];
        $category = Category::persistCategory($categoryData);

        $updatedData = [
            "name" => "electric devices",
            "is_active" => true
        ];

        $category = (new CategoriesAction())->updateCategory($category, $updatedData);
        $this->assertInstanceOf(Category::class, $category);
        $this->assertEquals("electric devices", $category->getOriginal("name"));
        $this->assertDatabaseCount("categories", 1);
        $this->assertDatabaseHas("categories", ["id" => 1]);
    }

    public function testUpdateMethodShouldValidateData(){
        $categoryData = [
            "name" => "electronics",
            "description" => "devices that run on electricity",
            "is_active" => true,
        ];

        $category = Category::persistCategory($categoryData);

        $updatedData = [
//            "name" => "electric devices",
            "is_active" => true
        ];

        $this->expectException(ValidationException::class);
        $updatedCategory = $category->updateCategory($updatedData);
    }

}
