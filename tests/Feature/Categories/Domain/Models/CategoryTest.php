<?php

namespace Categories\Domain\Models;

use App\Features\Categories\Domain\Models\Category;
use App\Features\Products\Domain\Models\Product;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    use DatabaseMigrations;

    public function testPersistMethodMustReturnStudentInstanceIfValidatedPassedSuccessfully()
    {
        $categoryData = [
            "name" => "electronics",
            "description" => "devices that run on electricity",
            "is_active" => true,
        ];
        $category = Category::persistCategory($categoryData);
        $this->assertInstanceOf(Category::class, $category);

        $productData = [
            "category_id" => $category->id,
            "name" => "iPhone",
            "description" => "Flagship",
            "price" => 100000,
            "stock" => 10,
            "is_active" => true,
        ];
        $product = Product::persistProduct($productData);
        $this->assertInstanceOf(Product::class, $product);

        //assertions
        $this->assertEquals("electronics", $category->getOriginal("name"));
        $this->assertDatabaseCount("categories", 1);
        $this->assertDatabaseHas("categories", ["id" => 1]);
        $this->assertEquals("iPhone", $product->getOriginal("name"));
        $this->assertDatabaseCount("products", 1);
        $this->assertDatabaseHas("products", ["id" => 1]);

    }

    public function testPersistMethodShouldValidateData(){
        $categoryData = [
//            "name" => "electronics",
            "description" => "devices that run on electricity",
            "is_active" => true,
        ];
        $this->expectException(ValidationException::class);
        $category = Category::persistCategory($categoryData);
    }

    public function testUpdateCategoryShouldReturnCategoryModelIfValidatedSuccessfully()
    {
        $categoryData = [
            "name" => "electronics",
            "description" => "devices that run on electricity",
            "is_active" => true,
        ];

        $category = Category::persistCategory($categoryData);

        $updatedData = [
            "name" => "electric devices",
            "is_active" => true
        ];
        $updatedCategory = $category->updateCategory($updatedData);
        $this->assertInstanceOf(Category::class,$updatedCategory);
    }

    public function testUpdateCategoryShouldValidateData()
    {
        $categoryData = [
            "name" => "electronics",
            "description" => "devices that run on electricity",
            "is_active" => true,
        ];

        $category = Category::persistCategory($categoryData);
        $updatedData = [
            "is_active" => true
        ];
        $this->expectException(ValidationException::class);
        $updatedCategory = $category->updateCategory($updatedData);
    }

    public function testProductsMethodMustReturnHasManyInstance()
    {
        $categoryData = [
            "name" => "electronics",
            "description" => "devices that run on electricity",
            "is_active" => true,
        ];
        $category = Category::persistCategory($categoryData);
        $products = $category->products();

        $this->assertInstanceOf(HasMany::class, $products);
    }

}
