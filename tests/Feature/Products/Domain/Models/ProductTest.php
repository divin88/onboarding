<?php

namespace Products\Domain\Models;

use App\Features\Categories\Domain\Models\Category;
use App\Features\Products\Domain\Models\Product;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use DatabaseMigrations;

    public function testPersistMethodMustReturnProductInstanceIfValidatePassedSuccessfully()
    {
        $categoryData = [
            "name" => "electronics",
            "description" => "electronics",
            "is_active" => true,
        ];

        $category = Category::persistCategory($categoryData);

        $productData = [
            "category_id" => $category->id,
            "name" => "iPhone",
            "description" => "Flagship",
            "price" => 100000,
            "stock" => 10,
            "is_active" => true,
        ];

        $product = Product::persistProduct($productData);

        //assertions
        $this->assertInstanceOf(Product::class, $product);
        $this->assertEquals("iPhone", $product->getOriginal("name"));
        $this->assertDatabaseCount("products", 1);
        $this->assertDatabaseHas("products", ["id" => 1]);

    }

    public function testPersistMethodShouldValidateData(){
        $categoryData = [
            "name" => "electronics",
            "description" => "electronics",
            "is_active" => true,
        ];

        $category = Category::persistCategory($categoryData);

        $productData = [
//            "category_id" => $category->id,
//            "name" => "iPhone",
            "description" => "Flagship",
            "price" => 100000,
            "stock" => 10,
            "is_active" => true,
        ];
        $this->expectException(ValidationException::class);
        $product = Product::persistProduct($productData);
    }

    public function testUpdateMethodShouldReturnProductModelIfValidatedSuccessfully()
    {
        $categoryData = [
            "name" => "electronics",
            "description" => "devices that run on electricity",
            "is_active" => true,
        ];
        $category = Category::persistCategory($categoryData);

        $productData = [
            "category_id" => $category->id,
            "name" => "iPhone",
            "description" => "Flagship",
            "price" => 100000,
            "stock" => 10,
            "is_active" => true,
        ];
        $product = Product::persistProduct($productData);

        $updatedData = [
            "category_id" => $category->id,
            "name" => "Samsung",
            "description" => "Midrange|Budget|Flagship",
            "price" => 50000,
            "stock" => 10,
            "is_active" => true,
        ];
        $updatedProduct = $product->updateProduct($updatedData);
        $this->assertInstanceOf(Product::class,$updatedProduct);
    }

    public function testUpdateCategoryShouldValidateData()
    {
        $categoryData = [
            "name" => "electronics",
            "description" => "devices that run on electricity",
            "is_active" => true,
        ];
        $category = Category::persistCategory($categoryData);

        $productData = [
            "category_id" => $category->id,
            "name" => "iPhone",
            "description" => "Flagship",
            "price" => 100000,
            "stock" => 10,
            "is_active" => true,
        ];
        $product = Product::persistProduct($productData);

        $updatedData = [
            "category_id" => $category->id,
            "name" => "Samsung",
            "description" => "Midrange|Budget|Flagship",
            "price",
            "stock" => 10,
            "is_active" => true,
        ];
        $this->expectException(ValidationException::class);
        $updatedProduct = $product->updateProduct($updatedData);
    }

    public function testCategoryMethodMustReturnBelongsToInstance()
    {
        $categoryData = [
            "name" => "electronics",
            "description" => "devices that run on electricity",
            "is_active" => true,
        ];
        $category = Category::persistCategory($categoryData);

        $productData = [
            "category_id" => $category->id,
            "name" => "iPhone",
            "description" => "Flagship",
            "price" => 100000,
            "stock" => 10,
            "is_active" => true,
        ];
        $product = Product::persistProduct($productData);
        $categories = $product->category();

        $this->assertInstanceOf(BelongsTo::class, $categories);
    }

}
