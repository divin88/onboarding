<?php

namespace Database\Factories;

use App\Features\Categories\Domain\Models\Category;
use App\Features\Products\Domain\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Product::class;
    public function definition(): array
    {
        return [
            "name" => fake()->word(),
            "category_id" => Category::pluck("id")->random(),
            "description" => fake()->paragraph(1),
            "price" => fake()->randomFloat(2),
            "stock" => fake()->numberBetween(1, 500),
            "is_active" => fake()->boolean(),
        ];
    }
}
