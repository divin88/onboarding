<?php

namespace Database\Factories;

use App\Features\Categories\Domain\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class CategoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Category::class;
    public function definition(): array
    {
        return [
            "name" => fake()->word(),
            "description" => fake()->paragraph(1),
            "is_active" => fake()->boolean(),
        ];
    }
}
